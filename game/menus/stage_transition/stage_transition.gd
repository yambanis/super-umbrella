extends Control

enum { DEATH, WIN }

var stage_controller = null
var pause_manager = null
var victory = false

func _ready():
	var finder = preload("res://managers/finder/Finder.gd").new(get_tree())
	
	assert(finder.has_pauser())
	pause_manager = finder.get_pauser()
	
	if finder.has_stage_controller():
		stage_controller = finder.get_stage_controller()

	$FadeTransition.fade_in()
	pause_manager.transition_pause()
	yield($FadeTransition, "fade_completed")
	pause_manager.transition_pause()


func transition(trigger):
	if trigger == DEATH:
		$Death_Label.show()
	elif trigger == WIN:
		$Win_Label.show()
		victory = true
	
	pause_manager.transition_pause()
	$Timer.start()

#Use with caution because of "Stage_Controller"
func fade_to_scene(scene):
	$FadeTransition.fade_out()
	yield($FadeTransition, "fade_completed")
	get_tree().change_scene(scene)

func _on_Timer_timeout():
	$FadeTransition.fade_out()
	yield($FadeTransition, "fade_completed")
	if stage_controller != null:
		if victory:
			stage_controller.save_and_exit()
		else:
			stage_controller.exit()
	else:
		get_tree().quit()
