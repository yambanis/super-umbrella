extends Control

const FADE_DURATION = .5

export(Color) var color = Color(.1, .1, .1)

signal fade_completed

func _ready():
	$Fade.color = color
	$Fade.visible = true


func fade(from, to):
	$Tween.interpolate_property($Fade, "color", from, to, FADE_DURATION,
		Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()


func fade_in():
	var from = color
	from.a = 1
	var to = color
	to.a = 0
	
	fade(from, to)


func fade_out():
	var from = color
	from.a = 0
	var to = color
	to.a = 1
	
	fade(from, to)


func _on_Tween_tween_completed(object, key):
	emit_signal("fade_completed")
